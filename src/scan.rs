use crate::consts::SUPPORTED_FORMATS;
use crate::human_readable;
use crate::progress_bar::scan_progress_style;
use anyhow::{Context, Error};
use bytesize::ByteSize;
use indicatif::{ProgressBar, ProgressIterator};
use log::debug;
use std::path::PathBuf;
use std::process::Command;
use walkdir::{DirEntry, WalkDir};

#[derive(Debug)]
pub struct MediaFile {
    pub path: PathBuf,
    pub size: u64,
    pub name: String,
    pub extension: String,
    pub duration: f64,
    pub frame_rate: f64,
    pub frame_count: f64,
}

pub struct SelectedItems {
    pub items: Vec<MediaFile>,
    pub initial_size: Vec<f64>,
}

pub struct FileScanner;

impl FileScanner {
    pub fn scan_dir(
        path: PathBuf,
        target: u64,
        season_mode: bool,
        disable_limit: bool,
    ) -> Result<SelectedItems, Error> {
        let target_pth: Vec<DirEntry> = WalkDir::new(path)
            .max_depth(if season_mode { 2 } else { 1 })
            .into_iter()
            .filter_map(|file| file.ok())
            .collect();

        let tgt_count = target_pth.len();

        let pb = ProgressBar::new(tgt_count as u64).with_style(scan_progress_style());

        let mut file_list: Vec<MediaFile> = Vec::new();
        let mut size_list: Vec<f64> = Vec::new();

        for i in target_pth.iter().progress_with(pb) {
            let file_sz = i
                .metadata()
                .with_context(|| "Could not read file metadata")?
                .len();
            if i.file_type().is_file() && (file_sz > target || disable_limit) {
                debug!("{} - {}", i.path().display(), human_readable!(file_sz));
                if let Some(extension_str) = i.path().extension() {
                    if let Some(extension) = extension_str.to_str() {
                        if SUPPORTED_FORMATS.contains(&extension) {
                            let file_path = PathBuf::from(i.path());

                            let dur = Self::get_duration(&file_path)?;
                            let fr = Self::get_frame_rate(&file_path)?;

                            let ms = MediaFile {
                                path: PathBuf::from(i.path()),
                                size: file_sz,
                                name: String::from(i.path().file_stem().unwrap().to_str().unwrap()),
                                extension: String::from(extension),
                                duration: dur,
                                frame_rate: fr,
                                frame_count: fr * dur,
                            };
                            file_list.push(ms);
                            size_list.push(file_sz as f64);
                        }
                    }
                }
            }
        }
        Ok(SelectedItems {
            items: file_list,
            initial_size: size_list,
        })
    }
    fn get_duration(path: &PathBuf) -> Result<f64, Error> {
        let mut ffprobe_cmd = Command::new("ffprobe");
        ffprobe_cmd.args([
            "-v",
            "quiet",
            "-show_entries",
            "format=duration",
            "-of",
            "default=noprint_wrappers=1:nokey=1",
        ]);
        ffprobe_cmd.arg(path);

        let out = String::from_utf8(
            ffprobe_cmd
                .output()
                .with_context(|| "Could not read ffprobe output")?
                .stdout,
        )?;
        let conv_float = out.trim().parse::<f64>().with_context(|| {
            "Failed to get video duration. File might be corrupted or video is too short."
        })?;
        debug!("duration: {}", conv_float);
        Ok(conv_float)
    }

    fn get_frame_rate(path: &PathBuf) -> Result<f64, Error> {
        let mut ffprobe_cmd = Command::new("ffprobe");
        ffprobe_cmd.args([
            "-v",
            "quiet",
            "-of",
            "default=noprint_wrappers=1:nokey=1",
            "-select_streams",
            "v:0",
            "-show_entries",
            "stream=r_frame_rate",
            "-of",
            "default=noprint_wrappers=1:nokey=1",
        ]);
        ffprobe_cmd.arg(path);

        let mut halfs: Vec<f64> = Vec::new();

        let out = String::from_utf8(
            ffprobe_cmd
                .output()
                .with_context(|| "Could not read ffprobe output")?
                .stdout,
        )?;
        let parts = out.trim().split('/');

        for i in parts {
            let part = i
                .parse::<f64>()
                .with_context(|| "Failed to get video frame rate. The file might be corrupted.")?;
            halfs.push(part);
        }
        let framerate = halfs[0] / halfs[1];
        debug!("fps: {}/n", out);
        Ok(framerate)
    }
}
