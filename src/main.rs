mod consts;
mod ffmpeg;
mod macros;
mod progress_bar;
mod scan;
mod util;

use crate::ffmpeg::FFMpeg;
use crate::util::simd_sum;
use anyhow::Error;
use clap::Parser;
use std::fs::canonicalize;
use std::path::PathBuf;

/// Program to encode video files to a specified size
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// Path to directory
    #[clap(value_parser)]
    pub path: Option<PathBuf>,

    /// Desired file size in MiB
    #[clap(short, long, value_parser, default_value_t = 600)]
    pub size: u16,

    /// Maximum file size deviation from target in MiB
    #[clap(short, long, value_parser, default_value_t = 100)]
    pub tolerance: u16,

    /// Desired audio bitrate (kbps)
    #[clap(short, long, value_parser, default_value_t = 64)]
    pub audio_bitrate: u8,

    /// Enable Series Mode
    #[clap(long, action)]
    pub series_mode: bool,

    /// Encode everything in folder ignoring all size constraints
    #[clap(short, long, action, help_heading = "Debug Options")]
    pub disable_limits: bool,

    /// Only scan dir and show results
    #[clap(long, action, help_heading = "Debug Options")]
    pub scan_only: bool,
}

fn main() -> Result<(), Error> {
    env_logger::builder().format_timestamp(None).init();
    let args = Args::parse();

    let path: PathBuf = match args.path {
        Some(path) => path,
        None => PathBuf::from("./"),
    };

    let full_path = canonicalize(&path)?;

    let ffm = FFMpeg::new(
        full_path,
        args.audio_bitrate,
        u64::from(args.size),
        u64::from(args.tolerance),
        args.series_mode,
        args.disable_limits,
    );
    if !args.scan_only {
        if let Ok(mut ffm) = ffm {
            ffm.run()?;
        }
    }
    Ok(())
}
