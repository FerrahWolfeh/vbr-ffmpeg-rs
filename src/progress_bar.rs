use indicatif::{ProgressBar, ProgressDrawTarget, ProgressState, ProgressStyle};
use once_cell::sync::OnceCell;
use std::fmt::Write;
use std::time::Duration;

const PROGRESS_CHARS: &str = "━━";

pub enum Pass {
    First,
    Second,
}

pub enum ProgressTemplates {
    Scan,
    FFMpeg,
}

impl ProgressTemplates {
    pub fn str(&self) -> &str {
        match self {
            ProgressTemplates::Scan => {"{spinner:.green.bold} {elapsed_precise:.bold} {wide_bar:.green/white.dim} {position:.green} ({files_sec:.bold} est. {eta})"}
            ProgressTemplates::FFMpeg => {"{spinner:.green.bold} {elapsed_precise:.bold} {wide_bar:.green/white.dim} {percent:.bold}  {pos:.green} ({fps:.bold}, est. {eta} | {msg:.blue.bold})"}
        }
    }
}

fn encode_progress_style() -> ProgressStyle {
    ProgressStyle::default_bar()
        .template(ProgressTemplates::FFMpeg.str())
        .unwrap()
        .with_key(
            "fps",
            |state: &ProgressState, w: &mut dyn Write| match state.per_sec() {
                fps if fps.abs() < f64::EPSILON => write!(w, "0 fps").unwrap(),
                fps if fps < 1.0 => write!(w, "{:.2} s/fr", 1.0 / fps).unwrap(),
                fps => write!(w, "{:.2} fps", fps).unwrap(),
            },
        )
        .with_key("pos", |state: &ProgressState, w: &mut dyn Write| {
            write!(w, "{}/{}", state.pos(), state.len().unwrap()).unwrap()
        })
        .with_key("percent", |state: &ProgressState, w: &mut dyn Write| {
            write!(w, "{:>3.0}%", state.fraction() * 100_f32).unwrap()
        })
        .progress_chars(PROGRESS_CHARS)
}

pub fn scan_progress_style() -> ProgressStyle {
    ProgressStyle::default_bar()
        .template(ProgressTemplates::Scan.str())
        .unwrap()
        .with_key(
            "files_sec",
            |state: &ProgressState, w: &mut dyn Write| match state.per_sec() {
                files_sec if files_sec.abs() < f64::EPSILON => write!(w, "0 files/s").unwrap(),
                files_sec if files_sec < 1.0 => write!(w, "{:.2} s/file", 1.0 / files_sec).unwrap(),
                files_sec => write!(w, "{:.2} files/s", files_sec).unwrap(),
            },
        )
        .with_key("position", |state: &ProgressState, w: &mut dyn Write| {
            write!(w, "{}/{}", state.pos(), state.len().unwrap()).unwrap()
        })
        .progress_chars(PROGRESS_CHARS)
}

pub fn init_progress_ffmpeg(bar: &OnceCell<ProgressBar>, len: u64, pass: Pass) {
    let pb = bar.get_or_init(|| ProgressBar::new(len).with_style(encode_progress_style()));
    pb.set_draw_target(ProgressDrawTarget::stderr_with_hz(60));
    pb.enable_steady_tick(Duration::from_millis(100));
    pb.reset();
    pb.reset_eta();
    pb.reset_elapsed();
    pb.set_position(0);

    match pass {
        Pass::First => {
            pb.set_message("1st Pass");
        }
        Pass::Second => {
            pb.set_message("2nd Pass");
        }
    }
}

pub fn set_pos(bar: &OnceCell<ProgressBar>, pos: u64) {
    if let Some(pb) = bar.get() {
        pb.set_position(pos);
    }
}

pub fn finish_progress_bar(bar: &OnceCell<ProgressBar>) {
    if let Some(pb) = bar.get() {
        pb.finish();
    }
}
