pub const READ_BYTE: u8 = b'\r';

pub const SUPPORTED_FORMATS: [&str; 6] = ["mkv", "mp4", "avi", "webm", "rmvb", "mov"];

pub const FFMPEG_INIT_ARGS: [&str; 6] =
    ["-hide_banner", "-loglevel", "error", "-stats", "-y", "-i"];

pub const FFMPEG_DEFAULT_ENCODER_PRESET: [&str; 8] = [
    "-c:v",
    "libx264",
    "-preset",
    "slow",
    "-tune",
    "film",
    "-profile:v",
    "high",
];

pub const FFMPEG_FIRST_PASS_ENDING: [&str; 6] = ["-an", "-pass", "1", "-f", "null", "/dev/null"];

pub const FFMPEG_DEFAULT_AUDIO_PRESET: [&str; 4] = ["-c:a", "libopus", "-ac", "2"];

pub const FFMPEG_SECOND_PASS_MAPPINGS: [&str; 14] = [
    "-map",
    "0:v:0",
    "-map",
    "0:a",
    "-map",
    "0:s:?",
    "-c:s",
    "ass",
    "-map_metadata",
    "-1",
    "-map_chapters",
    "-1",
    "-pass",
    "2",
];
