pub fn simd_sum(values: &[f64]) -> f64 {
    const LANES: usize = 16;
    let chunks = values.chunks_exact(LANES);
    let remainder = chunks.remainder();

    let sum = chunks.fold([0.0f64; LANES], |mut acc, chunk| {
        let chunk: [f64; LANES] = chunk.try_into().unwrap();
        for i in 0..LANES {
            acc[i] += chunk[i];
        }
        acc
    });

    let remainder: f64 = remainder.iter().copied().sum();

    let mut reduced = 0.0f64;
    for i in sum {
        reduced += i;
    }
    reduced + remainder
}
