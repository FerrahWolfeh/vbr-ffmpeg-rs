use crate::consts::*;
use crate::progress_bar::{finish_progress_bar, init_progress_ffmpeg, set_pos, Pass};
use crate::scan::{FileScanner, MediaFile, SelectedItems};
use crate::{human_readable, simd_sum};
use anyhow::{bail, Context, Error};
use bytesize::ByteSize;
use colored::Colorize;
use indicatif::ProgressBar;
use log::debug;
use once_cell::sync::OnceCell;
use regex::Regex;
use std::fs;
use std::io::{BufRead, BufReader};
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};
use std::thread::sleep;
use std::time::Duration;
use tempdir::TempDir;

pub struct FFMpeg {
    media_files: SelectedItems,
    target_video_size: u64,
    target_audio_br: u8,
    initial_size: f64,
    final_size: f64,
}

impl FFMpeg {
    pub fn new(
        path: PathBuf,
        target_a: u8,
        target_v: u64,
        tolerance: u64,
        season_mode: bool,
        disable_limit: bool,
    ) -> Result<Self, Error> {
        println!(
            "{} {:?}\n{} {}MiB",
            "Target dir:".bold(),
            &path,
            "Target size:".bold(),
            &target_v,
        );

        if disable_limit {
            println!("{}\n", "Scanning Limits Disabled".bold().yellow())
        } else {
            println!("{} {}MiB\n", "Size Tolerance:".bold(), &tolerance)
        }

        let full_list = FileScanner::scan_dir(
            path,
            (target_v + tolerance) * 1048576,
            season_mode,
            disable_limit,
        )?;

        if full_list.items.is_empty() {
            println!("{}", "No matching files found!".bold().yellow());
            bail!("")
        };

        let initial_size = simd_sum(&full_list.initial_size);

        println!(
            "{} {}\n{} {}\n\n",
            "Number of files found:".bold().bright_green(),
            full_list.items.len(),
            "Total size of files:".bold().yellow(),
            human_readable!(initial_size as u64)
        );

        Ok(Self {
            media_files: full_list,
            target_video_size: target_v,
            target_audio_br: target_a,
            initial_size,
            final_size: 0.0,
        })
    }

    pub fn run(&mut self) -> Result<(), Error> {
        let mut new_size: Vec<f64> = Vec::new();

        for file in &self.media_files.items {
            let temp_path = TempDir::new_in("/dev/shm", "ffmpeg-session")
                .unwrap()
                .into_path();

            debug!("{}{:?}", "Temp dir: ".bold(), temp_path);

            debug!(
                "Path: {:?}\nSize: {}\nName: {}\nExtension: {}\n",
                file.path,
                human_readable!(file.size),
                file.name,
                file.extension
            );

            let size_calc = (&self.target_video_size * 8196) as f64 / file.duration;

            if size_calc <= 10.0 {
                bail!("Video bitrate is impossibly small (calculated {:.2} kbps)\nConsider increasing target size", size_calc);
            };

            let corrected_v_br = size_calc - self.target_audio_br as f64;

            debug!("Calculated duration: {}", file.duration);
            debug!("Calculated target bitrate: {:.2}k", corrected_v_br);
            debug!("Calculated frame rate: {:.2}", file.frame_rate);
            debug!("Calculated frame count: {:.2}", file.frame_count);

            println!(
                "{} {} | {}",
                "Encoding file:".bold(),
                &file.path.display(),
                human_readable!(file.size).bold().blue()
            );
            Self::run_first_pass(file, corrected_v_br, &temp_path)?;
            sleep(Duration::from_secs_f32(0.3)); // wait a little to prevent the progress bar from exploding

            let sp = Self::run_second_pass(file, corrected_v_br, self.target_audio_br, &temp_path)?;

            let size_st = if (sp as u64) < file.size {
                human_readable!(sp as u64).green()
            } else {
                human_readable!(sp as u64).red()
            };

            println!(
                "└──{}  |  {} -> {}\n",
                "Encode Finished.".bold().green(),
                human_readable!(file.size).bold().blue(),
                size_st
            );

            new_size.push(sp);
        }

        self.final_size = simd_sum(&new_size);
        let delta_size = self.initial_size - self.final_size;

        let delta_string = if delta_size > 0.0 {
            format!("-{}", human_readable!(delta_size as u64)).green()
        } else {
            format!("+{}", human_readable!((delta_size * -1.0) as u64)).red()
        };

        let delta_percentage = ((self.final_size / self.initial_size) * 100.0) - 100.0;
        let percent_string = if delta_percentage < 0.0 {
            format!("({:.2}%)", delta_percentage).green()
        } else {
            format!("(+{:.2}%)", delta_percentage).red()
        };
        println!(
            "\n{}\n\n{} {}  {} {} | {} {} {}",
            "Encoding complete!".bold().green(),
            "Files now occupy:".bold().yellow(),
            human_readable!(self.final_size.round() as u64)
                .bold()
                .bright_blue(),
            "from:".bold().yellow(),
            human_readable!(self.initial_size.round() as u64).bold(),
            "Delta:".bold().yellow(),
            delta_string,
            percent_string
        );
        Ok(())
    }

    fn run_first_pass(
        file_data: &MediaFile,
        target_v: f64,
        temp_dir: &PathBuf,
    ) -> Result<(), Error> {
        let mut ffmpeg = Command::new("ffmpeg");
        ffmpeg.args(FFMPEG_INIT_ARGS);
        ffmpeg.arg(&file_data.path);
        ffmpeg.args(FFMPEG_DEFAULT_ENCODER_PRESET);
        ffmpeg.arg("-b:v");
        ffmpeg.arg(format!("{:.2}k", target_v));
        ffmpeg.args(FFMPEG_FIRST_PASS_ENDING);

        debug!("FFMpeg command: {:?}", ffmpeg);

        Self::ffmpeg(&mut ffmpeg, file_data.frame_count, temp_dir, Pass::First)
            .with_context(|| "Error in 1st pass")?;

        Ok(())
    }

    fn run_second_pass(
        file_data: &MediaFile,
        target_v: f64,
        target_a: u8,
        temp_dir: &PathBuf,
    ) -> Result<f64, Error> {
        let mut ffmpeg = Command::new("ffmpeg");
        ffmpeg.args(FFMPEG_INIT_ARGS);
        ffmpeg.arg(&file_data.path);
        ffmpeg.args(FFMPEG_DEFAULT_ENCODER_PRESET);
        ffmpeg.arg("-b:v");
        ffmpeg.arg(format!("{:.2}k", target_v));
        ffmpeg.args(FFMPEG_DEFAULT_AUDIO_PRESET);
        ffmpeg.arg("-b:a");
        ffmpeg.arg(format!("{}k", target_a));
        ffmpeg.args(FFMPEG_SECOND_PASS_MAPPINGS);
        let ffmpeg_out = format!(
            "{}.mkv",
            Path::new(&temp_dir).join(&file_data.name).display(),
        );
        ffmpeg.arg(&ffmpeg_out);

        debug!("FFMpeg command: {:?}", ffmpeg);

        Self::ffmpeg(&mut ffmpeg, file_data.frame_count, temp_dir, Pass::Second)
            .with_context(|| "Error in 2nd pass")?;

        let out_file_size = Path::new(&ffmpeg_out).metadata()?.len() as f64;
        let dir = file_data.path.parent().unwrap().to_str().unwrap();
        if &file_data.extension != "mkv" || &file_data.extension != "webm" {
            debug!("Deleting file: {}", &file_data.path.display());
            fs::remove_file(&file_data.path)?;
            debug!(
                "{} -> {}",
                &ffmpeg_out,
                format!("{}/{}.mkv", dir, file_data.name)
            );
            fs::copy(
                Path::new(&ffmpeg_out),
                format!("{}/{}.mkv", dir, file_data.name),
            )?;
        } else {
            debug!("{} -> {}", &ffmpeg_out, &file_data.path.display());
            fs::copy(Path::new(&ffmpeg_out), &file_data.path)?;
        }
        debug!("Deleting temp_dir: {}", &temp_dir.display());
        fs::remove_dir_all(&temp_dir)?;

        Ok(out_file_size)
    }

    fn ffmpeg(
        com: &mut Command,
        frame_count: f64,
        tmp_path: &PathBuf,
        pass: Pass,
    ) -> Result<(), Error> {
        let bar: OnceCell<ProgressBar> = OnceCell::new();
        let ffmpeg = com
            .stderr(Stdio::piped())
            .current_dir(tmp_path)
            .spawn()
            .with_context(|| "Failed to start ffmpeg.")?
            .stderr
            .with_context(|| "Failed to capture ffmpeg output")?;

        let mut reader = BufReader::new(ffmpeg);

        let progress_rx = Regex::new(r"frame=\s?+(\d+)")?;

        init_progress_ffmpeg(&bar, frame_count as u64, pass);

        loop {
            let mut buf = vec![];
            reader.read_until(READ_BYTE, &mut buf)?;

            if let Ok(line) = String::from_utf8(buf) {
                let std_line = &line;

                if std_line.is_empty() {
                    break;
                }

                if let Some(x) = progress_rx.captures_iter(std_line).next() {
                    let current = x
                        .get(1)
                        .unwrap()
                        .as_str()
                        .parse::<u64>()
                        .with_context(|| "Failed to parse progress")?;

                    set_pos(&bar, current);
                    sleep(Duration::from_secs_f32(0.1));
                }
            } else {
                finish_progress_bar(&bar);
                break;
            }
        }

        Ok(())
    }
}
